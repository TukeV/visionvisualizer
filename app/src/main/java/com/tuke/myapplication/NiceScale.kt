package com.tuke.myapplication

class NiceScale(private var minPoint: Double, private var maxPoint: Double) {

    private var maxTicks = 10.0
    var tickSpacing: Double = 0.toDouble()
    var range: Double = 0.toDouble()
    var niceMin: Double = 0.toDouble()
    var niceMax: Double = 0.toDouble()

    init {
        calculate()
    }

    /**
     * Calculate and update values for tick spacing and nice
     * minimum and maximum data points on the axis.
     */
    private fun calculate() {
        this.range = niceNum(maxPoint - minPoint, false)
        this.tickSpacing = niceNum(range / (maxTicks - 1), true)
        this.niceMin = Math.floor(minPoint / tickSpacing) * tickSpacing
        this.niceMax = Math.ceil(maxPoint / tickSpacing) * tickSpacing
    }

    /**
     * Returns a "nice" number approximately equal to range Rounds
     * the number if round = true Takes the ceiling if round = false.
     *
     * @param range the data range
     * @param round whether to round the result
     * @return a "nice" number to be used for the data range
     */
    private fun niceNum(range: Double, round: Boolean): Double {
        val exponent: Double = Math.floor(Math.log10(range))
        /** exponent of range  */
        val fraction: Double
        /** fractional part of range  */
        val niceFraction: Double
        /** nice, rounded fraction  */

        fraction = range / Math.pow(10.0, exponent)

        if (round) {
            if (fraction < 1.5)
                niceFraction = 1.0
            else if (fraction < 3)
                niceFraction = 2.0
            else if (fraction < 7)
                niceFraction = 5.0
            else
                niceFraction = 10.0
        } else {
            if (fraction <= 1)
                niceFraction = 1.0
            else if (fraction <= 2)
                niceFraction = 2.0
            else if (fraction <= 5)
                niceFraction = 5.0
            else
                niceFraction = 10.0
        }
        return niceFraction * Math.pow(10.0, exponent)
    }

    /**
     * Sets the minimum and maximum data points for the axis.
     *
     * @param minPoint the minimum data point on the axis
     * @param maxPoint the maximum data point on the axis
     */
    fun setMinMaxPoints(minPoint: Double, maxPoint: Double) {
        this.minPoint = minPoint
        this.maxPoint = maxPoint
        calculate()
    }

    /**
     * Sets maximum number of tick marks we're comfortable with
     *
     * @param maxTicks the maximum number of tick marks for the axis
     */
    fun setMaxTicks(maxTicks: Double) {
        this.maxTicks = maxTicks
        calculate()
    }
}