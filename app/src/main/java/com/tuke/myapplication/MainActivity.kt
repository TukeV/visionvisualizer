package com.tuke.myapplication

import android.Manifest
import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import com.github.mikephil.charting.charts.CombinedChart
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.widget.CheckBox


class MainActivity : AppCompatActivity() {

    lateinit var polarView: PolarView
    lateinit var toggleScatter: CheckBox
    lateinit var toggleWindowed: CheckBox
    lateinit var optionsMenu: Menu

    companion object {
        private const val FILE_CHOOSER_ACTIVITY = 1
        private const val CHECK_PERMISSIONS = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val chart = findViewById<CombinedChart>(R.id.chart)
        polarView = PolarView(chart)
        polarView.drawPolarAxis()
        checkPermissions()

        toggleScatter = findViewById(R.id.scatter_checkbox)
        toggleScatter.setOnCheckedChangeListener { _, isChecked -> polarView.showScatter = isChecked}
        toggleWindowed = findViewById(R.id.line_checkbox)
        toggleWindowed.setOnCheckedChangeListener { _, isChecked -> polarView.showWindow = isChecked}
        toggleScatter.isEnabled = false
        toggleWindowed.isEnabled = false
    }

    private fun checkPermissions(){
        if(ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), CHECK_PERMISSIONS)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        optionsMenu = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.find_file -> {
                val intent = Intent()
                intent.type = "file/*"
                val mimeTypes = arrayOf("text/csv", "text/comma-separated-values", "application/pdf")
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(
                    Intent.createChooser(intent, getText(R.string.select_file)),
                    FILE_CHOOSER_ACTIVITY
                )
                return true
            }
            R.id.clear -> {
                polarView.removeDataSet()
                toggleWindowed.isEnabled = false
                toggleScatter.isEnabled = false
                optionsMenu.findItem(R.id.clear).isEnabled = false
            }
            R.id.help -> Toast.makeText(applicationContext, "Help Clicked", Toast.LENGTH_SHORT).show()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when(requestCode){
            FILE_CHOOSER_ACTIVITY -> {
                if(resultCode == Activity.RESULT_OK) {
                    val uri = data?.data
                    if (uri != null) {
                        val inputStream = contentResolver.openInputStream(uri)
                        if (inputStream != null) {
                            val visionData = VisionData(inputStream)
                            polarView.addDataSet(visionData)
                        }
                    }
                    toggleWindowed.isEnabled = true
                    toggleScatter.isEnabled = true
                    optionsMenu.findItem(R.id.clear).isEnabled = true
                }
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }
}
