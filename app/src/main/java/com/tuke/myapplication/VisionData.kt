package com.tuke.myapplication

import android.graphics.Color
import com.github.mikephil.charting.charts.ScatterChart
import com.github.mikephil.charting.data.*
import java.io.InputStream
import kotlin.math.cos
import kotlin.math.max
import kotlin.math.min
import kotlin.math.sin


class VisionData(stream: InputStream){
    lateinit var rawDataSet: ScatterDataSet

    lateinit var windowedDataSetPlus: LineDataSet
    lateinit var windowedDataSetMinus: LineDataSet

    private val fileData = mutableListOf<PolarCoordinate>()

    data class PolarCoordinate(val theta: Double, val radius: Double)

    var maximumRadius = Double.MIN_VALUE
    var minimumRadius = Double.MAX_VALUE

    init {
        stream.bufferedReader().useLines { lines -> lines.forEach {
            it.trim()
            val splat = it.split(',')
            if(splat.size == 3){
                val dataPoint = PolarCoordinate(splat[0].toDouble(), splat[2].toDouble())
                fileData.add(dataPoint)
            }
        }}
        createRawData()
        calculateWindowedAverage()
    }

    private fun createRawData(){
        val entries = mutableListOf<Entry>()
        for(dataPoint in fileData){
            val radius = dataPoint.radius
            maximumRadius = max(maximumRadius, radius)
            minimumRadius = min(minimumRadius, radius)
            val x = (radius * cos(dataPoint.theta)).toFloat()
            val y = (radius * sin(dataPoint.theta)).toFloat()
            entries.add(Entry(x, y))
        }
        entries.sortBy{it.x}
        rawDataSet = ScatterDataSet(entries, "Original Data")
        rawDataSet.isHighlightEnabled = true
        rawDataSet.setScatterShape(ScatterChart.ScatterShape.CROSS)
        rawDataSet.color = Color.CYAN
        rawDataSet.scatterShapeSize = 20f
    }

    private fun calculateWindowedAverage(windowSize: Int = 11){
        val entriesPlus = mutableListOf<Entry>()
        val entriesMinus = mutableListOf<Entry>()
        assert(fileData.size > windowSize)
        var i0 = fileData.size - Math.floor(windowSize/2.0).toInt()
        var i1 = Math.floor(windowSize/2.0).toInt()
        var avg = windowedAverage(i0, i1)
        for(polar in fileData){
            val x = avg * cos(polar.theta)
            val y = avg * sin(polar.theta)
            if(y > 0){
                entriesPlus.add(Entry(x.toFloat(), y.toFloat()))
            }else{
                entriesMinus.add(Entry(x.toFloat(), y.toFloat()))
            }
            avg -= (fileData[i0].radius/windowSize)
            i0 = (i0 + 1) % fileData.size
            i1 = (i1 + 1) % fileData.size
            avg += (fileData[i1].radius/windowSize)
        }
        entriesPlus.sortBy { it.x }
        entriesMinus.add(entriesPlus.first())
        entriesMinus.add(entriesPlus.last())
        entriesMinus.sortBy{ it.x }
        windowedDataSetMinus = LineDataSet(entriesMinus, "")
        windowedDataSetPlus = LineDataSet(entriesPlus, "")

        windowedDataSetMinus.isHighlightEnabled = false
        windowedDataSetMinus.color = Color.GREEN
        windowedDataSetPlus.isHighlightEnabled = false
        windowedDataSetPlus.color = Color.GREEN

        windowedDataSetMinus.setDrawCircles(false)
        windowedDataSetPlus.setDrawCircles(false)

    }

    private fun windowedAverage(i0: Int, i1: Int): Double{
        if(i0 > i1){
            val sub1 = fileData.subList(i0, fileData.size)
            val sub2 = fileData.subList(0, i1)
            return (sub1.sumByDouble { it.radius } + sub2.sumByDouble { it.radius }) / (sub1.size + sub2.size).toDouble()
        }
        val window = fileData.subList(i0, i1)
        return window.sumByDouble { it.radius } / window.size.toDouble()
    }

    fun clear(){
        rawDataSet.clear()
        windowedDataSetPlus.clear()
        windowedDataSetMinus.clear()
        fileData.clear()
    }

}