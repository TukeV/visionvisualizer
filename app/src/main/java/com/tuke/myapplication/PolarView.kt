package com.tuke.myapplication

import android.graphics.Color
import android.graphics.Typeface
import com.github.mikephil.charting.charts.CombinedChart
import com.github.mikephil.charting.data.*
import kotlin.math.*


class PolarView(private val chart: CombinedChart){

    companion object {
        val ANGLES_IN_CIRCLE = arrayOf(
            .0,
            Math.PI*.25,
            Math.PI*.5,
            Math.PI*.75,
            Math.PI,
            Math.PI*1.25,
            Math.PI*1.5,
            Math.PI*1.75
        )
    }

    private var chartData: CombinedData = CombinedData()
    private var lineData: LineData = LineData()
    private var visiondata: VisionData? = null
    private var niceScale = NiceScale(.0, 60.0)

    var showScatter = true
    set(value){
        field = value
        visiondata?.let {
            if(!value){
                chartData.removeDataSet(it.rawDataSet)
            }else{
                val scatterData = ScatterData(it.rawDataSet)
                chartData.setData(scatterData)
            }
            chart.invalidate()
        }
    }
    var showWindow = false
    set(value){
        field = value
        visiondata?.let {
            if(!value){
                lineData.removeDataSet(it.windowedDataSetMinus)
                lineData.removeDataSet(it.windowedDataSetPlus)
            }else{
                lineData.addDataSet(it.windowedDataSetPlus)
                lineData.addDataSet(it.windowedDataSetMinus)
            }
            chartData.setData(lineData)
            chart.invalidate()
        }
    }

    fun drawPolarAxis(){
        chart.setBackgroundColor(Color.BLACK)
        chart.setPinchZoom(false)
        chart.legend.isEnabled = false
        chart.isDragEnabled = false
        chart.isDoubleTapToZoomEnabled = false

        chart.axisLeft.isEnabled = false

        chart.axisRight.setDrawGridLines(false)
        chart.axisRight.setDrawLabels(true)
        chart.axisRight.textColor = Color.WHITE
        chart.axisRight.textSize = 15f

        chart.xAxis.setDrawGridLines(false)
        chart.xAxis.setDrawLabels(true)
        chart.xAxis.textColor = Color.WHITE
        chart.xAxis.textSize = 15f

        chart.data = chartData
        lineData = buildLines()
        chartData.setData(lineData)
        chart.data = chartData
        chartData.setValueTypeface(Typeface.SANS_SERIF)
        chart.resetViewPortOffsets()
        chart.xAxis.spaceMin = niceScale.tickSpacing.toFloat()*.5f
        chart.xAxis.spaceMax = niceScale.tickSpacing.toFloat()*.5f
        chart.invalidate()
    }

    private fun buildLines(minPoint: Double=niceScale.niceMin, maxPoint: Double=niceScale.niceMax): LineData{
        niceScale.setMinMaxPoints(minPoint, maxPoint)
        val lineData = LineData()
        // Create lines
        for(angle in ANGLES_IN_CIRCLE){
            val dataSet = createRadialLine(angle)
            lineData.addDataSet(dataSet)
        }
        // Create circles
        var radial = niceScale.niceMin
        while(radial <= niceScale.niceMax){
            lineData.addDataSet(createRadialCircle(radial.toFloat(), true))
            lineData.addDataSet(createRadialCircle(radial.toFloat(), false))
            radial += niceScale.tickSpacing
        }
        return lineData
    }

    private fun createRadialLine(angle: Double): LineDataSet{
        val entries = mutableListOf<Entry>()
        var radial = niceScale.niceMin
        while(radial <= niceScale.niceMax){
            val x = radial * cos(angle)
            val y = radial * sin(angle)
            entries.add(Entry(x.toFloat(), y.toFloat()))
            radial += niceScale.tickSpacing
        }
        entries.sortBy {it.x}
        val lineDataSet = LineDataSet(entries, "")
        lineDataSet.setDrawCircles(false)
        lineDataSet.color = Color.DKGRAY
        lineDataSet.isHighlightEnabled = true
        return lineDataSet
    }

    private fun createRadialCircle(radius: Float, direction: Boolean): LineDataSet {
        val entries = mutableListOf<Entry>()
        var angle = PI
        while(0 <= angle && angle <= 2*PI){
            val x = radius * cos(angle)
            val y = radius * sin(angle)
            entries.add(Entry(x.toFloat(), y.toFloat()))
            angle += if(direction) .1 else -.1
        }
        entries.add(Entry((radius*cos(.0)).toFloat(), (radius*sin(.0).toFloat())))
        val circleDataSet = LineDataSet(entries, "")
        circleDataSet.color = Color.DKGRAY
        circleDataSet.setDrawCircles(false)
        circleDataSet.setDrawValues(false)
        circleDataSet.isHighlightEnabled = false
        return circleDataSet
    }

    fun addDataSet(dataSet: VisionData){
        chart.clear()
        chartData.clearValues()
        chart.invalidate()
        visiondata = dataSet
        niceScale.setMinMaxPoints(dataSet.minimumRadius, dataSet.maximumRadius)
        lineData = buildLines()
        if(showScatter){
            val scatterData = ScatterData(dataSet.rawDataSet)
            chartData.setData(scatterData)
        }
        if(showWindow){
            lineData.addDataSet(dataSet.windowedDataSetPlus)
            lineData.addDataSet(dataSet.windowedDataSetMinus)
        }
        chartData.setData(lineData)
        chart.data = chartData
        chart.resetViewPortOffsets()
        chart.xAxis.spaceMin = niceScale.tickSpacing.toFloat()*.5f
        chart.xAxis.spaceMax = niceScale.tickSpacing.toFloat()*.5f
        chart.invalidate()
    }

    fun removeDataSet(){
        chartData.removeDataSet(visiondata?.rawDataSet)
        lineData.removeDataSet(visiondata?.windowedDataSetMinus)
        lineData.removeDataSet(visiondata?.windowedDataSetPlus)
        visiondata?.clear()
        chart.invalidate()
        visiondata = null
        chart.resetViewPortOffsets()
        chart.invalidate()
    }
}